---

---
{{< figure height="300" src="/images/office1.jpg" class="left" >}}

## 20+ Years of Great Service

We specialize in quality, affordable small to medium size business IT support 
that bridges the gap between business and technology. Not only do we provide 
superior customer service, we also act as your trusted technology adviser ensuring 
the best technology investments to produce the highest return on investment. 

---
{{< wrapper about-us >}}
## About Us

ITS is a locally owned IT Service company. We believe in long term relationships with clients throughout the product lifecycle, from network design and equipment sourcing, to setup, installation, and ongoing support. We provide prompt attention to all your computer and network issues.
{{< /wrapper >}}

--- 
{{< wrapper about-us >}}
## Services

Since 1997, we have provided both onsite and remote technical support to end users and SMB clients. We source and service top brand hardware. We provide software installation services such as Medical EMR setup, Sage Accounting and Quickbook Accounting updates and installations, and Data migration services. We help our clients feel confident in their purchase and support decisions, including helping with their leasing arrangements through third party providers. We strive to reduce the risk of failures and data loss. We investigate weaknesses in client networks and make recommendations on ways to medicate risks. We assist and manage office relocation and moves to avoid unnecessary downtime due to delays in technology sourcing and network interruptions.

{{< /wrapper >}}

---

{{< figure height="475" src="/images/office02.jpg" class="right" >}}

{{< wrapper products >}}
## Products

- Window Servers
- Desktops
- Laptops
- Software products and Microsoft Office Suite
- Printers and Toners
- Scanners
- Network equipment (Routers, Switches, Cabling)
- WiFi Access Points
- Video and Security Cameras
- Inventory Clearance Specials
- ...
{{< /wrapper >}}


---

{{< clear >}}

{{< center >}}{{< bold >}}Contact us at your convenience:
{{< center >}}{{% bold %}}Address:{{% /bold %}} 630 Old Sackville Road, Lower Sackville, B4C2K3
{{< center >}}{{% bold %}}Phone:{{% /bold %}} (902) 452-6487
{{< center >}}{{% bold %}}Email:{{% /bold %}}urban@itshalifax.com
{{< center >}}{{% rs_button %}}
{{< contact >}}
{{< center >}}{{< bold >}}Please allow us 12 to 24 hours to get back to you.

{{< hr >}}

{{< center >}}{{< enlarge >}}ITS is an Authorized Reseller for:
{{< figure src="/images/hp.png" class="center" link="https://www8.hp.com" target="">}}
{{< figure src="/images/lenovo.png" class="center" height="45" link="http://www.lenovo.com" target="">}}
{{< figure src="/images/buffalo.jpg" class="center" height="30" link="https://www.buffalo-technology.com/" target="">}}
